# history

date purchased: 2012-08-01

miles: ~109000


## maintainence

date        | miles     | tasks
----------- | --------- | -------------------------------------------
2015-11-22  | 123160    | change oil
2015-03-15  | 121000    | replace front left headlamp
2015-10-18  | 120000    | replace fuel filter
2014-09-18  | 118500    | replace fuel pre-filter
2014-09-15  | 118500    | change oil & replace filter
            |           | replace air filter
2014-01-23  | 115425    | change oil & replace filter
            |           | replace front brake pads & rotors
            |           | replace rear brake pads
            |           | replace two engine mounts
            |           | replace transmission mount & oil pan gasket
            |           | replace upper control arms
            |           | replace tie rod ends
